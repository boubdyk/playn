package playn.WorkPlease.WorkPlease.core;

import static playn.core.PlayN.*;
import playn.core.Game;
import playn.core.Image;
import playn.core.ImageLayer;
import playn.core.Pointer;

public class WorkPlease extends Game.Default {
	
	public static int angle = 5;
	public static float xPea = -230, yPea = -30;

	public WorkPlease() {
		super(33); // call update every 33ms (30 times per second)
	}

	@Override
	public void init() {
		// create and add background image layer
		Image bgImage = assets().getImage("images/bg.png");
		ImageLayer bgLayer = graphics().createImageLayer(bgImage);
		graphics().rootLayer().add(bgLayer);

		Image pea = assets().getImage("images/pea.png");
		final ImageLayer peaLayer = graphics().createImageLayer(pea);

		pointer().setListener(new Pointer.Adapter() {

			@Override
			public void onPointerEnd(Pointer.Event event) {
				xPea = -event.localX();
				yPea = -event.localY();
				
//				System.out.println(event.x() + "\n" + event.y());
				System.out.println(event.localX() + "\n" + event.localY());
				peaLayer.setOrigin(xPea, yPea);

			}
		});

		graphics().rootLayer().add(peaLayer);
	}

	@Override
	public void update(int delta) {
	}

	@Override
	public void paint(float alpha) {
		// the background automatically paints itself, so no need to do anything
		// here!
	}
}
