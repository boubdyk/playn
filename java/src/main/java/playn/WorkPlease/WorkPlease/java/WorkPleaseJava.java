package playn.WorkPlease.WorkPlease.java;

import playn.core.PlayN;
import playn.java.JavaPlatform;

import playn.WorkPlease.WorkPlease.core.WorkPlease;

public class WorkPleaseJava {

  public static void main(String[] args) {
    JavaPlatform.Config config = new JavaPlatform.Config();
    // use config to customize the Java platform, if needed
    JavaPlatform.register(config);
    PlayN.run(new WorkPlease());
  }
}
