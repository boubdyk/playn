package playn.WorkPlease.WorkPlease.android;

import playn.android.GameActivity;
import playn.core.PlayN;

import playn.WorkPlease.WorkPlease.core.WorkPlease;

public class WorkPleaseActivity extends GameActivity {

  @Override
  public void main(){
    PlayN.run(new WorkPlease());
  }
}
